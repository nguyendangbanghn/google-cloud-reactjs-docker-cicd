# Set the base image to node:12-alpine
FROM node:12-alpine as build

# Chỉ định nơi ứng dụng sẽ chạy trong container
WORKDIR /app

# Copy ứng dụng vào vùng chứa
COPY . /app/

# Chuẩn bị container để chạy react.
RUN npm install
RUN npm install react-scripts@3.0.1 -g
# Build
RUN npm run build

# Chuẩn bị nginx
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Khởi động nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
